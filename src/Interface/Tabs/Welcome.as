class WelcomeTab : Tab {
    string GetLabel() override { return "Welcome"; }
    bool CanClose() { return true; }

    string welcomeText =
"""
 # Welcome to Haptics!

__Disclaimer:__ This plugin is in *proof-of-concept* state. I am not responsible for any possible damages or unwanted actions this plugin may bring. Be careful of your surroundings and use at your own risk!

__Quick Setup:__ Run [Intiface Desktop](https://github.com/intiface/intiface-desktop/releases) and [Haptics Web Bridge](https://openplanet.dev/file/102) for this plugin to work properly and turn on your devices (connect manually if needed). You can manually specify the address, port and key in the plugin settings *(Openplanet > Settings > Haptics > Server)*. If you need full installation instructions, please visit the [Installation](https://gitlab.com/fentrasLABS/openplanet/haptics/-/wikis/Installation) page on GitLab.

Have fun!

""";

    void Render() override
    {
        UI::Markdown(welcomeText);
        Setting_ShowWelcome = UI::Checkbox("Show this screen on startup", Setting_ShowWelcome);
    }
}