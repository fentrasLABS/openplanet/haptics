// The whole plugin is a proof-of-concept and is going 
// to be rewritten when WebSockets are released for Openplanet.

const string title = "\\$f09" + Icons::Kenney::JoystickRight + "\\$z Haptics";

Game@ game;
Window@ window;

void RenderMenu() {
    if (UI::MenuItem(title)) {
        window.isOpened = !window.isOpened;
    }
}

void RenderInterface()
{
    if (window.isOpened) {
		window.Render();
	}
}

void Render()
{
	if (game.initialised) {
		if (game.app.GameScene is null) {
			game.Initialise(false);
		}
	} else {
		game.Initialise();
	}
}

void Update(float dt)
{
    API::Update(dt);
    if (game !is null) {
        game.Update(dt);
    }
}

void Main()
{
	@game = Game();
	@window = Window();
}
