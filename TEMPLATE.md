# Plugin Description Template

### openplanet.dev
* Description
* Disclaimer
* Installation
* [Source Code](#source-code)
* Credits

#### Source Code
You can access source code on [GitLab](https://gitlab.com/fentrasLABS/openplanet/haptics).

---

### fentras.com
* Disclaimer
* Installation
* Screenshots
* [Download](#download)
* Credits

#### Download
* [Releases](https://gitlab.com/fentrasLABS/openplanet/haptics/-/releases)

##### Plugin
* [Openplanet](https://openplanet.dev/plugin/haptics)
* [Source](https://gitlab.com/fentrasLABS/openplanet/haptics)

##### Web Bridge
* [Openplanet](https://openplanet.dev/file/102)
* [Source](https://gitlab.com/fentrasLABS/openplanet/haptics-web-bridge)

